package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"sort"
	"strconv"
)

var n int

func init() {
	flag.IntVar(&n, "n", 0, "the largest number")
	flag.Parse()
}

func main() {
	f, err := os.Open("test.txt")
	scanner := bufio.NewScanner(f)
	top := make([]int, n)
	err, top = topN(top, scanner)
	if err != nil {
		panic(err)
	}
	fmt.Println("Final result ", top)
}

func topN(top []int, s *bufio.Scanner) (error, []int) {
	for i := 0; i < len(top); i++ {
		if s.Scan() {
			u, err := strconv.Atoi(s.Text())
			if err != nil {
				return err, []int{}
			}
			top[i] = u
		}
	}
	sort.Ints(top)
	sort.Sort(sort.Reverse(sort.IntSlice(top)))
	for s.Scan() {
		u, err := strconv.Atoi(s.Text())
		if err != nil {
			return err, []int{}
		}
		top = Insert(u, top)
	}
	return nil, top
}
func Insert(value int, target []int) []int {
	size := len(target)
	for i := 0; i < len(target); i++ {
		if target[i] < value {
			target = append(target, 0)
			copy(target[i+1:], target[i:])
			target[i] = value
			break
		}
	}
	return append([]int(nil), target[:size]...)
}
