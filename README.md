# TopN Algo

Write a program, topN, that given an arbitrarily large file and a number, N, containing individual numbers on each line (e.g. 200Gb file), will output the largest N numbers, highest first. Tell me about the run time/space complexity of it, and whether you think there's room for improvement in your approach.

Usage

```
Mauros-MacBook-Pro:tests maurodelazeri$ go run main.go -n 1
Final result  [19]
```

```
Mauros-MacBook-Pro:tests maurodelazeri$ go run main.go -n 2
Final result  [19 18
```